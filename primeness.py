

# My (slower) version.
def slower_is_prime(n):
    if n < 2:
        return False

    # If a number n is not a prime, it can be factored into two factors a and b:
    # n = a*b
    # If both a and b were greater than the square root of n, a*b would be greater than n.
    # So at least one of those factors must be less than or equal to the square root of n,
    # and to check if n is prime, we only need to test for factors less than or equal to the square root.
    # https://stackoverflow.com/a/5811176
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False

    return True


# This is a faster, more optimized version.
# Taken from https://stackoverflow.com/a/15285588
def is_prime(n):
    if n == 2 or n == 3:
        return True

    if n % 2 == 0 or n < 2:
        return False

    for i in range(3, int(n**0.5) + 1, 2):   # only odd numbers
        if n % i == 0:
            return False

    return True
