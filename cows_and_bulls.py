
from collections import Counter
from sys import exit


def string_to_digits(string):
    try:
        return [int(c) for c in str(string)]
    except ValueError:
        print('Could not convert {} into digits, exiting...'.format(string))
        exit(-1)


# Inspired by https://stackoverflow.com/a/17317412 and https://stackoverflow.com/a/20007822
def calculate_cows_and_bulls(secret, users_guess):
    """
    Calculates the number of cows and bulls in dependence of
    the randomly chosen secret and a user provided number.
    """

    # '1234' => [1, 2, 3, 4]
    secret_digits = string_to_digits(secret)
    guess_digits = string_to_digits(users_guess)

    # An example:
    # secret_digits:   [1, 2, 3, 4]
    # guess_digits:    [1, 3, 2, 4]
    # cow_tuples:      [(1,1), (4,4)]
    # number_of_cows:  2
    cow_tuples = [sg for sg in zip(secret_digits, guess_digits) if sg[0] == sg[1]]
    number_of_cows = len(cow_tuples)

    # Counter([1,1,1]) => Counter({1: 3})
    # Counter([1, 1, 1]) & Counter([1]) => Counter({1: 1})
    # Counter([1, 1, 1]) & Counter([1, 1]) => Counter({1: 2})
    #
    # Counter({1: 1, 2: 1, 3: 1, 4: 1}) = Counter({1: 1, 2: 1, 3: 1, 4: 1}) & Counter({1: 1, 3: 1, 2: 1, 4: 1})
    # number_of_common_digits: 4
    common_digits_with_freq = Counter(secret_digits) & Counter(guess_digits)
    number_of_common_digits = sum(common_digits_with_freq.values())

    # number_of_bulls = 4 - 2 = 2
    number_of_bulls = number_of_common_digits - number_of_cows

    return number_of_cows, number_of_bulls
