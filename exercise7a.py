
def overlap(first_list, second_list):
    """
    Returns the overlap among the two lists.
    The overlap refers to all elements found in both lists.
    """
    intersection_set = set(first_list).intersection(second_list)
    intersection_list = list(intersection_set)
    return intersection_list

