#!/usr/bin/env python3


def main():
    while True:
        try:
            message = 'Please enter a number to check (it will be called "num") (quit with a non-number character): '
            num = int(input(message))
        except ValueError:
            # The conversion of the message (into an integer) was unsuccessful.
            # This is because the user wants to quit by entering a non-number character.
            print('Quitting...')
            break

        try:
            message = 'Please enter a number to divide with (it will be called "check") (quit with a non-number character): '
            check = int(input(message))
        except ValueError:
            print('Quitting...')
            break

        check_divides_evenly_into_num = num % check == 0

        if check_divides_evenly_into_num:
            print('check ({0}) divides evenly into num ({1})'.format(check, num))
        else:
            print('check ({0}) does not divide evenly into num ({1})'.format(check, num))


if __name__ == '__main__':
    main()
