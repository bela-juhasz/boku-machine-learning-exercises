
from math import log, e
from functools import reduce


def to_uppercase1(string):
    """Converts all characters in a string to upper case."""
    return string.upper()


def to_uppercase2(string):
    """Converts all characters in a string to upper case."""
    uppercase_chars = [char.upper() for char in string]
    uppercase_string = ''.join(uppercase_chars)
    return uppercase_string


def to_uppercase3(string):
    """Converts all characters in a string to upper case."""
    uppercase_chars = map(lambda char: char.upper(), string)
    uppercase_string = ''.join(uppercase_chars)
    return uppercase_string


def to_uppercase4(string):
    """Converts all characters in a string to upper case."""
    return reduce(lambda char1, char2: char1.upper() + char2.upper(), string)


def sum_of_entries1(input_list):
    """Calculates the sum of entries in a list."""
    return sum(input_list)


def sum_of_entries2(input_list):
    """Calculates the sum of entries in a list."""
    return reduce(lambda x, y: x + y, input_list)


def log_factorial(number, log_base=e):
    """Calculates the log factorial of an integer number."""

    # n! = 1 * 2 * 3 * ... * n
    # log(n!) = log(1 * 2 * 3 * ... * n) = log(1) + log(2) + log(3) + ... + log(n).
    return sum(map(lambda x: log(x, log_base), range(1, number+1)))
