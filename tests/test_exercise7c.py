#!/usr/bin/env python3

import unittest
from exercise7c import unique_overlap


class TestStringMethods(unittest.TestCase):

    def test_unique_overlap(self):
        self.assertEqual(([1, 2], 0, 1), unique_overlap([1, 2], [0, 1, 2, 0]))

        self.assertEqual(([1, 2], 1, 0), unique_overlap([0, 1, 2, 0], [1, 2]))

        self.assertEqual(([2], 0, 2), unique_overlap([2, 1], [0, 1, 2, 0]))

        self.assertEqual(([], 0, 0), unique_overlap([2, 1], [0]))

        self.assertEqual(([1], 0, 1), unique_overlap([1, 2], [0, 1, 0, 2, 0]))

        self.assertEqual(([1, 2], 0, 0), unique_overlap([1, 2, 9, 9, 1, 2], [1, 2]))

        self.assertEqual(([1, 2], 0, 0), unique_overlap([1, 2], [1, 2, 9, 9, 1, 2]))

        self.assertEqual(([1, 2], 0, 4), unique_overlap([1, 2, 3, 4], [3, 4, 9, 9, 1, 2]))


if __name__ == '__main__':
    unittest.main()
