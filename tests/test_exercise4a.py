#!/usr/bin/env python3

import unittest
from primeness import is_prime, slower_is_prime


class TestIsPrime(unittest.TestCase):

    expected_number_of_primes_between_1_and_1000 = 168

    def test_is_prime(self):
        primes_between_1_and_1000 = [i for i in range(1, 1001) if is_prime(i)]

        actual_number_of_primes_between_1_and_1000 = len(primes_between_1_and_1000)

        self.assertEqual(self.expected_number_of_primes_between_1_and_1000, actual_number_of_primes_between_1_and_1000)

        print('The primes between 1 and 1000:\n{}\n'.format(primes_between_1_and_1000))

    def test_slower_is_prime(self):
        primes_between_1_and_1000 = [i for i in range(1, 1001) if slower_is_prime(i)]

        actual_number_of_primes_between_1_and_1000 = len(primes_between_1_and_1000)

        self.assertEqual(self.expected_number_of_primes_between_1_and_1000, actual_number_of_primes_between_1_and_1000)

        #print('The primes between 1 and 1000:\n{}\n'.format(primes_between_1_and_1000))


if __name__ == '__main__':
    unittest.main()

