#!/usr/bin/env python3

import unittest
from exercise5 import reverse_sentence1, reverse_sentence2


class TestReverseSentence(unittest.TestCase):

    original_sentence = 'Perl is an acronym for Pathologically Eclectic Rubbish Lister'
    expected_reversed_sentence = 'Lister Rubbish Eclectic Pathologically for acronym an is Perl'

    def test_reverse_sentence1(self):
        actual_reversed_sentence = reverse_sentence1(self.original_sentence)

        self.assertEqual(self.expected_reversed_sentence, actual_reversed_sentence)

    def test_reverse_sentence2(self):
        actual_reversed_sentence = reverse_sentence2(self.original_sentence)

        self.assertEqual(self.expected_reversed_sentence, actual_reversed_sentence)


if __name__ == '__main__':
    unittest.main()

