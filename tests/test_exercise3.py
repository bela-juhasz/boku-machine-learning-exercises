#!/usr/bin/env python3

import unittest
from exercise3 import unique_entries1, unique_entries2


class TestUniqueEntries(unittest.TestCase):

    def test_unique_entries1_with_numbers(self):
        duplicate_entries = [11, 1, 12, 2, 9, 1, 13, 12, 2, 4, 6]

        actual_unique_entries = unique_entries1(duplicate_entries)

        actual_unique_entries_sorted = sorted(actual_unique_entries)
        expected_unique_entries = [1, 2, 4, 6, 9, 11, 12, 13]
        self.assertEqual(expected_unique_entries, actual_unique_entries_sorted)

    def test_unique_entries1_with_strings(self):
        duplicate_entries = ['Zita', 'Anna', 'Cornelia', 'Anton', 'Joseph', 'Anna', 'Maria', 'Anton', 'Cornelia']

        actual_unique_entries = unique_entries1(duplicate_entries)

        actual_unique_entries_sorted = sorted(actual_unique_entries)
        expected_unique_entries = ['Anna', 'Anton', 'Cornelia', 'Joseph', 'Maria', 'Zita']
        self.assertEqual(expected_unique_entries, actual_unique_entries_sorted)

    def test_unique_entries2_with_numbers(self):
        duplicate_entries = [11, 1, 12, 2, 9, 1, 13, 12, 2, 4, 6]

        actual_unique_entries = unique_entries2(duplicate_entries)

        expected_unique_entries = [11, 9, 1, 13, 12, 2, 4, 6]
        self.assertEqual(expected_unique_entries, actual_unique_entries)

    def test_unique_entries2_with_strings(self):
        duplicate_entries = ['Zita', 'Anna', 'Cornelia', 'Anton', 'Joseph', 'Anna', 'Maria', 'Anton', 'Cornelia']

        actual_unique_entries = unique_entries2(duplicate_entries)

        expected_unique_entries = ['Zita', 'Joseph', 'Anna', 'Maria', 'Anton', 'Cornelia']
        self.assertEqual(expected_unique_entries, actual_unique_entries)


if __name__ == '__main__':
    unittest.main()
