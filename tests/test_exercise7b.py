#!/usr/bin/env python3

import unittest
from exercise7b import overlap_using_list_comprehension


class TestStringMethods(unittest.TestCase):

    def test_overlap_using_list_comprehension(self):
        self.assertEqual([1], overlap_using_list_comprehension([1, 1], [1, 1, 1, 1]))

        self.assertEqual([1], overlap_using_list_comprehension([1, 2], [1, 1, 1, 1]))

        self.assertEqual([1], overlap_using_list_comprehension([1, 9, 1, 9, 1], [1, 2]))

        self.assertEqual([1, 2], overlap_using_list_comprehension([1, 2], [0, 1, 2, 0]))

        self.assertEqual([1, 2], overlap_using_list_comprehension([0, 1, 2, 0], [1, 2]))

        self.assertEqual([1, 2], overlap_using_list_comprehension([2, 1], [0, 1, 2, 0]))

        self.assertEqual([1, 2], overlap_using_list_comprehension([1, 2], [0, 1, 0, 2, 0]))

        self.assertEqual([1, 2], overlap_using_list_comprehension([1, 2, 0, 0, 1, 2], [1, 2]))

        self.assertEqual([1, 2], overlap_using_list_comprehension([1, 2], [1, 2, 0, 0, 1, 2]))


if __name__ == '__main__':
    unittest.main()
