#!/usr/bin/env python3

import unittest
from exercise2b import calculate_approximate_ages


class TestCalculatingAges(unittest.TestCase):

    def test_calculate_approximate_ages(self):
        years_of_birth = [1990, 1991, 1990, 1990, 1992, 1991, 1980]

        actual_approximate_ages = calculate_approximate_ages(years_of_birth)

        expected_approximate_ages = [28, 27, 28, 28, 26, 27, 38]
        self.assertEqual(expected_approximate_ages, actual_approximate_ages)


if __name__ == '__main__':
    unittest.main()
