#!/usr/bin/env python3

import unittest
from math import log, factorial, isclose
from exercise8 import to_uppercase1, to_uppercase2, to_uppercase3, to_uppercase4
from exercise8 import sum_of_entries1, sum_of_entries2, log_factorial


class TestFunctions(unittest.TestCase):

    expected_uppercase_string = 'ABC DEF'

    def test_to_uppercase1(self):
        self.assertEqual(self.expected_uppercase_string, to_uppercase1('abc Def'))

    def test_to_uppercase2(self):
        self.assertEqual(self.expected_uppercase_string, to_uppercase2('abc Def'))

    def test_to_uppercase3(self):
        self.assertEqual(self.expected_uppercase_string, to_uppercase3('abc Def'))

    def test_to_uppercase4(self):
        self.assertEqual(self.expected_uppercase_string, to_uppercase4('abc Def'))

    expected_sum_of_entries = 7.5

    def test_sum_of_entries1(self):
        self.assertEqual(self.expected_sum_of_entries, sum_of_entries1([0, 1, 2, 3, -3, 4.5]))

    def test_sum_of_entries2(self):
        self.assertEqual(self.expected_sum_of_entries, sum_of_entries2([0, 1, 2, 3, -3, 4.5]))

    def test_log_factorial(self):
        actual_log_factorial = log_factorial(19)

        expected_log_factorial = log(factorial(19)) # using built-in functions
        self.assertEqual(expected_log_factorial, actual_log_factorial)

    def test_log_factorial_with_custom_log_base(self):
        actual_log_factorial = log_factorial(19, 10)

        expected_log_factorial = log(factorial(19), 10) # using built-in functions
        self.assertTrue(isclose(expected_log_factorial, actual_log_factorial))


if __name__ == '__main__':
    unittest.main()
