#!/usr/bin/env python3

import unittest
from exercise2a import filter_even


class TestFilterEven(unittest.TestCase):

    def test_filter_even(self):
        input_list = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

        actual_filtered_list = filter_even(input_list)

        expected_filtered_list = [4, 16, 36, 64, 100]
        self.assertEqual(expected_filtered_list, actual_filtered_list)


if __name__ == '__main__':
    unittest.main()
