#!/usr/bin/env python3

import unittest
from cows_and_bulls import calculate_cows_and_bulls


class TestCowsAndBulls(unittest.TestCase):

    def test_cows_and_bulls1(self):
        # cows_and_bulls(secret, users_guess)
        cows, bulls = calculate_cows_and_bulls('1234', '1234')

        self.assertEqual(4, cows)
        self.assertEqual(0, bulls)

    def test_cows_and_bulls2(self):
        # cows_and_bulls(secret, users_guess)
        cows, bulls = calculate_cows_and_bulls('4444', '4444')

        self.assertEqual(4, cows)
        self.assertEqual(0, bulls)

    def test_cows_and_bulls3(self):
        # cows_and_bulls(secret, users_guess)
        cows, bulls = calculate_cows_and_bulls('4271', '1234')

        self.assertEqual(1, cows)
        self.assertEqual(2, bulls)

    def test_cows_and_bulls4(self):
        # cows_and_bulls(secret, users_guess)
        cows, bulls = calculate_cows_and_bulls('1367', '3223')

        self.assertEqual(0, cows)
        self.assertEqual(1, bulls)

    def test_cows_and_bulls5(self):
        # cows_and_bulls(secret, users_guess)
        cows, bulls = calculate_cows_and_bulls('3223', '1367')

        self.assertEqual(0, cows)
        self.assertEqual(1, bulls)

    def test_cows_and_bulls6(self):
        # cows_and_bulls(secret, users_guess)
        cows, bulls = calculate_cows_and_bulls('7337', '3223')

        self.assertEqual(0, cows)
        self.assertEqual(2, bulls)

    def test_cows_and_bulls7(self):
        # cows_and_bulls(secret, users_guess)
        cows, bulls = calculate_cows_and_bulls('1234', '4321')

        self.assertEqual(0, cows)
        self.assertEqual(4, bulls)

    def test_cows_and_bulls8(self):
        # cows_and_bulls(secret, users_guess)
        cows, bulls = calculate_cows_and_bulls('1234', '1324')

        self.assertEqual(2, cows)
        self.assertEqual(2, bulls)


if __name__ == '__main__':
    unittest.main()

