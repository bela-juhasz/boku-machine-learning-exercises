#!/usr/bin/env python3

import unittest
from exercise7a import overlap


class TestStringMethods(unittest.TestCase):

    def test_overlap(self):
        self.assertEqual([1], overlap([1, 9, 1, 9, 1], [1, 2]))

        self.assertEqual([1, 2], overlap([1, 2], [0, 1, 2, 0]))

        self.assertEqual([1, 2], overlap([0, 1, 2, 0], [1, 2]))

        self.assertEqual([1, 2], overlap([2, 1], [0, 1, 2, 0]))

        self.assertEqual([1, 2], overlap([1, 2], [0, 1, 0, 2, 0]))

        self.assertEqual([1, 2], overlap([1, 2, 0, 0, 1, 2], [1, 2]))

        self.assertEqual([1, 2], overlap([1, 2], [1, 2, 0, 0, 1, 2]))


if __name__ == '__main__':
    unittest.main()
