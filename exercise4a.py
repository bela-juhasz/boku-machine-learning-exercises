#!/usr/bin/env python3

from primeness import is_prime


def main():
    primes_between_1_and_1000 = [i for i in range(1, 1001) if is_prime(i)]

    print('The primes between 1 and 1000:\n{}\n'.format(primes_between_1_and_1000))


if __name__ == '__main__':
    main()
