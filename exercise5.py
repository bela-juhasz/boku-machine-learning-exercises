#!/usr/bin/env python3

import re


def reverse_sentence1(sentence):
    """Reverses the order of words in a string."""
    whitespace_regex = re.compile('\s')
    words = whitespace_regex.split(sentence)
    reversed_words = list(reversed(words))
    reversed_sentence = ' '.join(reversed_words)
    return reversed_sentence


def reverse_sentence2(sentence):
    """Reverses the order of words in a string."""

    # If sep is not specified or is None, a different splitting algorithm is applied:
    # runs of consecutive whitespace are regarded as a single separator,
    # and the result will contain no empty strings at the start or end
    # if the string has leading or trailing whitespace.
    words = sentence.split()

    reversed_words = list(reversed(words))
    reversed_sentence = ' '.join(reversed_words)
    return reversed_sentence


def main():

    # Sub-task #2.
    original_sentence = 'Perl is an acronym for Pathologically Eclectic Rubbish Lister'
    reversed_sentence = reverse_sentence1(original_sentence)
    print('{0} => {1}\n'.format(original_sentence, reversed_sentence))

    # Sub-task #3.
    while True:
        sentence = input('Please enter a sentence to be reversed (quit by pressing enter): ')

        user_pressed_enter = sentence.strip() == ''

        if user_pressed_enter:
            print('Quitting...')
            break
        else:
            print(reverse_sentence1(sentence))


if __name__ == '__main__':
    main()
