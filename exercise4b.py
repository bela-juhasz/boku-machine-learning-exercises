#!/usr/bin/env python3

from primeness import is_prime


def main():
    while True:
        try:
            number = int(input('Please enter a number to test its primeness (quit with a non-number character): '))
        except ValueError:
            print('Quitting...')
            break
        else:
            if is_prime(number):
                print('{} is a prime number'.format(number))
            else:
                print('{} is not a prime number'.format(number))


if __name__ == '__main__':
    main()
