from difflib import SequenceMatcher


# Inspired by https://stackoverflow.com/a/39404777
def unique_overlap(first_list, second_list):
    """
    Determines the overlap of two lists with unique entries
    and also returns the positions of the overlap in both original lists.
    :param first_list:
    :param second_list:
    :return: a tuple containing the overlap and the positions of the overlap in both original lists
    """

    matcher = SequenceMatcher(a=first_list, b=second_list)
    match = matcher.find_longest_match(0, len(first_list), 0, len(second_list))

    start_of_overlap_in_first_list = match.a
    start_of_overlap_in_second_list = match.b
    length_of_overlap = match.size

    overlap = first_list[start_of_overlap_in_first_list:start_of_overlap_in_first_list+length_of_overlap]

    return overlap, start_of_overlap_in_first_list, start_of_overlap_in_second_list

