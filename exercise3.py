#!/usr/bin/env python3


def unique_entries1(input_list):
    """
    Converts a list to a list of unique entries.
    The order of the resulting list is undefined.
    """
    return list(set(input_list))


def unique_entries2(input_list):
    """
    Converts a list to a list of unique entries.
    Preserves the order of the input_list.
    """
    list_with_only_unique_entries = []

    for index, item in enumerate(input_list):
        next_index = index + 1
        rest_of_the_input_list = input_list[next_index:]
        if item not in rest_of_the_input_list:
            list_with_only_unique_entries.append(item)

    return list_with_only_unique_entries


def main():
    list1 = [11, 1, 12, 2, 9, 1, 13, 12, 2, 4, 6]
    print('{0} => {1}'.format(list1, unique_entries1(list1)))

    list2 = ['Zita', 'Anna', 'Cornelia', 'Anton', 'Joseph', 'Anna', 'Maria', 'Anton', 'Cornelia']
    print('{0} => {1}'.format(list2, unique_entries1(list2)))


if __name__ == '__main__':
    main()
