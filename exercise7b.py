
def overlap_using_list_comprehension(first_list, second_list):
    """
    Returns the overlap among the two lists.
    The overlap refers to all elements found in both lists.
    """
    common_elements_with_possible_duplicates = [x for x in first_list if x in second_list]
    common_elements_without_duplicates = set(common_elements_with_possible_duplicates)
    common_elements_without_duplicates_in_a_list = list(common_elements_without_duplicates)
    return common_elements_without_duplicates_in_a_list

