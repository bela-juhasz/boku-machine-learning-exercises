#!/usr/bin/env python3


def is_even(num):
    """Tests whether the given number is even or not."""
    return num % 2 == 0


def is_multiple_of_four(num):
    """Tests whether the given number is a multiple of 4 or not."""
    return num % 4 == 0


def main():
    while True:
        try:
            message = 'Please enter a number to test if it\'s odd or even (quit with a non-number character): '
            number = int(input(message))
        except ValueError:
            # The conversion of the message (into an integer) was unsuccessful.
            # This is because the user wants to quit by entering a non-number character.
            print('Quitting...')
            break
        else:
            # There was no exception, we indeed received a number.
            if is_multiple_of_four(number):
                print('{} is a multiple of 4'.format(number))
            elif is_even(number):
                print('{} is an even number'.format(number))
            else:
                print('{} is an odd number'.format(number))


if __name__ == '__main__':
    main()
