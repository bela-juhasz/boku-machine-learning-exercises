#!/usr/bin/env python3

from random import randint
from sys import exit
from cows_and_bulls import calculate_cows_and_bulls


def main():
    random_number = randint(0, 9999)
    secret = '{0:04d}'.format(random_number)

    print('I thought of an integer with 4 digits (hint: {}), try to guess it!'.format(secret))

    while True:
        users_guess = input('Please enter your 4-digit guess (quit by pressing enter): ')

        user_pressed_enter = users_guess.strip() == ''

        if user_pressed_enter:
            print('Quitting...')
            exit(0)

        number_of_cows, number_of_bulls = calculate_cows_and_bulls(secret, users_guess)

        if number_of_cows == 4:
            print('Congratulations, you guessed it!\n')

            # Let's play another game.
            # This can cause a stack overflow if the user plays long enough.
            main()
        else:
            print('Wrong guess. Cows: {0} and Bulls: {1}'.format(number_of_cows, number_of_bulls))


if __name__ == '__main__':
    main()
