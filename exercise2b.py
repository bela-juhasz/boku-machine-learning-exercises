#!/usr/bin/env python3

import datetime


def calculate_approximate_ages(years_of_birth):
    """
    Takes a list which contains years and calculates the approximate age
    (ignoring the month of birth) of all people born in the respective years.
    Calculation is done with respect to the current year.
    """
    now = datetime.datetime.now()
    current_year = now.year
    return [current_year - year_of_birth for year_of_birth in years_of_birth]


def main():
    years_of_birth = [1990, 1991, 1990, 1990, 1992, 1991]
    approximate_ages = calculate_approximate_ages(years_of_birth)
    print('{0} => {1}'.format(years_of_birth, approximate_ages))


if __name__ == '__main__':
    main()
