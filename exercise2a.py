#!/usr/bin/env python3


def filter_even(input_list):
    """Returns a new list that has only the even elements of the input_list."""
    return [i for i in input_list if i % 2 == 0]


def main():
    input_list = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
    filtered_list = filter_even(input_list)
    print('{0} => {1}'.format(input_list, filtered_list))


if __name__ == '__main__':
    main()
